/*
Chrono.h
Libreria para controlar tiempo de espera sin usar delay()
Autor: @fenixbinario
*/


#ifndef Chrono_h
#define Chrono_h

#define LOOP "Loop(); -> dentro de loop(), actualiza en cada ciclo el valor time, posteriormente usado en Hasta(); e Durante(); \n"
#define HASTA "Hasta(); -> Al contrario que Durante(); devuelve cierto solo cuando ha transcurrido el tiempo en milisegundos pasados por parametro \n"
#define OnDURANTE "Durante(); -> activa manualmente Durante(); \n"
#define DURANTE "Durante(); -> Al contrario que Hasta(); delvuelve cierto durante los milisegundos pasados por parametro, luego se desactiva automaticamente. \n"
#define MOSTRAR "Mostrar(); -> Muestra info de private time\n"
#define MAS "Info(); -> Muestra la informacion sobre la clase y los miembros \n"
#define INFO  MAS LOOP HASTA OnDURANTE DURANTE

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#ifdef __AVR_ATtiny85__
 #include "TinyWireM.h"
 #define Wire TinyWireM
#else
 #include <Wire.h>
#endif

class Chrono
{
  protected:
  unsigned long time = 0;
  unsigned long contadorEspera = 0;
  unsigned long contadorDurante = 0;

  bool EncendidoDurante = true;
  
  public:
  Chrono();
  bool begin();
  void Loop(void);
  
  void Durante(void);
  bool Durante(unsigned long);
  
  bool Hasta(unsigned long);
  
  void Mostrar(void);
  
  void Info(void);
 
};


#endif

