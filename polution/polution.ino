/*
POLUTION 2018
CAQI: https://www.airqualitynow.eu/es/about_indices_definition.php
Site: www.transpeciessociety.com
Autor: @fenixbinario  @mat_osm
Librerias: Chrono.h 
*/
#include "chrono.h"
#include "MQ135.h"

//Incluir librerias de mq135

Chrono reloj;
Chrono pulso;

const int gasPin = A0;
const int gasDigiPin = 2;
int m1 = 6;
long cont = 0;
long duration, distance, actual, vibra=0, Escala_0_125=0 ;
long espera = 1500;
bool State = false;
bool activar = true;

//LED Indicador Escala Calidad del Aire
int muyBajo = 7; //0-25 Azul
int Bajo = 8; //25-50 Verde
int Medio = 9;//50-75 Amarillo
int Alto = 11; //75-100 Naranja
int muyAlto = 12; //100-125 Rojo
int animacion [] = {muyBajo,  Bajo, Medio, Alto,  muyAlto};

byte altavoz = 10;

void apagarLed(void)
{
  for(int x =0; x<5; x++)
  {
    digitalWrite(animacion[x], LOW);
  } 
}

void ledIndicador (long escala)
{

  apagarLed();

  if(escala > 0) {  digitalWrite(muyBajo, HIGH);  }
  if(escala >=25) {  digitalWrite(Bajo, HIGH);  }
  if(escala >=50) {  digitalWrite(Medio, HIGH); }
  if(escala >=75) {  digitalWrite(Alto, HIGH); }
  if(escala >=100){  digitalWrite(muyAlto, HIGH);  }

}

void bocina (void)
{
  
  noTone(altavoz);
  tone(altavoz, 3940, 200);
  delay(200);

  noTone(altavoz);
  tone(altavoz, 140, 500);
  delay(500);

  noTone(altavoz);
  tone(altavoz, 1923, 300);
  delay(300);
}


void setup(){
  ledIndicador(150);
  
  reloj.begin();
  pulso.begin();
  Serial.begin(9600);
  pinMode(m1,OUTPUT);
  digitalWrite(m1, LOW);
  pinMode(gasDigiPin,INPUT);
  reloj.Info();
}

void loop(){
reloj.Loop();
 
if( activar  )
 {  
  if (cont != actual)
  {
    Serial.println("Ahora " + String(cont) + " | Ultima lectura   " + String(actual));
    
    actual = cont;
    
    vibra = map(cont, 0,850,125,0);
    Escala_0_125 = map(cont, 0,850,0,125);
    
    //vibra = 100;
    if (cont >= 10 && cont <= 125)
      {
        Serial.print(cont);
        Serial.println(" ppm");
        Serial.print(vibra);
        Serial.println(" bpm");
        Serial.print(Escala_0_125);
        Serial.println(" Escala");
        Serial.println("");
        ledIndicador(Escala_0_125);
        reloj.Durante();
      }
      else 
      {
        //reloj.Durante();
         //Serial.print(String(cont) + " . ");
      }
      
  }
  else
  {
    if ( reloj.Hasta(500)  )
    {
      //reloj.Info();
      
      //reloj.Mostrar();

      
      cont = analogRead(gasPin);
      
      Serial.print("Gas:  ");
      Serial.println(cont, DEC);
      Serial.print("GasDigi:  ");
      Serial.println(digitalRead(gasDigiPin));
      Serial.println("");
      Serial.print(vibra);
      Serial.println(" bpm");
      if(Escala_0_125 >= 100)
        {
          bocina();
        }
    }
  }
 }  
 if(  reloj.Durante(espera) ) 
   {   
      if ( reloj.Hasta(vibra) )
        {
          if( !State == HIGH )
             {
              State = HIGH;  // ciclon *on*
              //Serial.print ( String(State) );
             }
          else
            {
             State = LOW;   // ciclon *off*
             //Serial.print ( String(State) );
            }
        }
     digitalWrite(m1, State);
     activar = false;
    }
    else
    {
      activar = true;
      digitalWrite(m1, LOW);
    }
   
}
